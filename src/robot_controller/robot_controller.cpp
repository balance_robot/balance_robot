#include "robot_controller.hpp"
#include <iostream>
#include <ostream>

using std::cout;
using std::endl;

Robot_Controller::Robot_Controller(float dt)
{
  _controller_dt = dt;
  // _p_start = p_start;

  _initPublishers();
  _initSubscribers();

  _p_odom << 0.0, 0.0, 0.0;

  t_start = ros::Time::now();

  _params.m = 0.0963;
  _params.r = 0.068 / 2.0;
  _params.Jw = 0.024 * _params.r * _params.r;
  _params.M = 0.5444;
  _params.h = 0.195 / 2.0;
}

Robot_Controller::~Robot_Controller()
{
}

void Robot_Controller::_initPublishers()
{
  _pub_joint_states = _nh.advertise<sensor_msgs::JointState>("/joint_states", 1);
  _pub_cmd = _nh.advertise<balance_robot_msgs::cmd>("/cmd", 1);
  _pub_data = _nh.advertise<balance_robot_msgs::data>("/data", 1);
  _pub_odom = _nh.advertise<nav_msgs::Odometry>("/odom", 1);
  _pub_imu = _nh.advertise<sensor_msgs::Imu>("/imu", 1);
  _pub_ground_truth_odom = _nh.advertise<nav_msgs::Odometry>("/ground_truth_odom", 1);
  _pub_ref_traj = _nh.advertise<nav_msgs::Path>("/reference_trajectory", 1);
}

void Robot_Controller::_initSubscribers()
{
  _sub_cmd_vel = _nh.subscribe("/cmd_vel", 1, &Robot_Controller::_cmdVelCallback, this);
}

void Robot_Controller::_cmdVelCallback(geometry_msgs::Twist msg)
{
  _cmd.v_cmd(0) = msg.linear.x * 0.4;
  _cmd.omega_des(2) = msg.angular.z * 3.0;
}

void Robot_Controller::_filterInput()
{
  float f = 0.05;
  static float v_cmd_prev = 0;

  _cmd.v_cmd(0) = v_cmd_prev * (1.0 - f) + f * _cmd.v_cmd(0);
  v_cmd_prev = _cmd.v_cmd(0);
}

double Robot_Controller::_getTime()
{
  // cout << "t_ms: " << (ros::Time::now() - t_start).toSec() << endl;
  return (ros::Time::now() - t_start).toSec();
}

void Robot_Controller::run()
{
  if (_is_frist_run)
  {
    _is_frist_run = false;
    _p_start = Eigen::vector3fToPoint(_data.p_act);
    _rpy_start = _data.rpy;
  }

  _filterInput();
  _evalOdom();
  _trajectoryPredicton();
  // _control1();
  // _lqrV2H();

  _lqrV2C();
  // _lqr_matlab();
  // _mpc();

  _updateVisualization();
  _updatePlot();
}

void Robot_Controller::setData(Data data)
{
  _data = data;
}

Command Robot_Controller::getControl()
{
  return _cmd;
}

Eigen::Matrix<float, 6, 1> Robot_Controller::getOdom()
{
  Eigen::Matrix<float, 6, 1> odom;

  odom.block<3, 1>(0, 0) = _data.p_odom;
  odom.block<3, 1>(3, 0) = _data.v_odom;
  return odom;
}

void Robot_Controller::_updateVisualization()
{
  sensor_msgs::JointState msg;

  msg.header.stamp = ros::Time::now();

  msg.name.push_back("bottom_to_right_wheel");
  msg.name.push_back("bottom_to_left_wheel");

  msg.position.push_back(_data.r_q);
  msg.velocity.push_back(_data.r_dq);
  msg.position.push_back(_data.l_q);
  msg.velocity.push_back(_data.l_dq);

  _pub_joint_states.publish(msg);

  geometry_msgs::TransformStamped odom_trans;
  geometry_msgs::TransformStamped tf_world_odom;

  tf_world_odom.header.stamp = ros::Time::now();
  tf_world_odom.header.frame_id = "world";
  tf_world_odom.child_frame_id = "odom";
  tf_world_odom.transform.rotation.w = 1.0;
  tf_world_odom.transform.translation.x = _p_start.x;
  tf_world_odom.transform.translation.y = _p_start.y;
  tf_world_odom.transform.translation.z = 0.0;
  _tf_world_odom_broadcaster.sendTransform(tf_world_odom);

  odom_trans.header.stamp = ros::Time::now();
  odom_trans.header.frame_id = "odom";
  odom_trans.child_frame_id = "base_link";

  odom_trans.transform.translation.x = _data.p_act(0) - _p_start.x;
  odom_trans.transform.translation.y = _data.p_act(1) - _p_start.y;
  odom_trans.transform.translation.z = _data.p_act(2);

  geometry_msgs::Quaternion odom_quat;
  odom_quat.x = _data.quat(1);
  odom_quat.y = _data.quat(2);
  odom_quat.z = _data.quat(3);
  odom_quat.w = _data.quat(0);
  odom_trans.transform.rotation = odom_quat;

  _odom_broadcaster.sendTransform(odom_trans);
}

void Robot_Controller::_trajectoryPredicton()
{
  uint8_t steps_num = 10;
  float dt = 0.1;

  _referenceTrajectory.poses.clear();
  _referenceTrajectory.header.frame_id = "base_link";
  _referenceTrajectory.header.stamp = ros::Time::now();

  Eigen::Vector3f p_hat(0, 0, 0);
  Eigen::Vector3f rpy_hat(0, 0, 0);

  for (size_t i = 0; i < steps_num; i++)
  {
    geometry_msgs::PoseStamped pose_of_traj;

    rpy_hat(2) += _cmd.omega_des(2) * dt;

    tf::Quaternion quat(tf::Vector3(0, 0, 1), rpy_hat(2));
    tf::Matrix3x3 Rot(quat);
    Eigen::Matrix<float, 3, 3> R;

    for (uint8_t i = 0; i < 3; i++)
    {
      for (uint8_t j = 0; j < 3; j++)
      {
        R(i, j) = Rot[i][j];
      }
    }

    p_hat = p_hat + R * _cmd.v_des * dt;
    pose_of_traj.pose.position = Eigen::vector3fToPoint(p_hat);
    pose_of_traj.pose.orientation.w = _data.quat(0);
    pose_of_traj.pose.orientation.x = _data.quat(1);
    pose_of_traj.pose.orientation.y = _data.quat(2);
    pose_of_traj.pose.orientation.z = _data.quat(3);

    _referenceTrajectory.poses.push_back(pose_of_traj);
  }

  _pub_ref_traj.publish(_referenceTrajectory);
}

void Robot_Controller::_updatePlot()
{
  balance_robot_msgs::cmd cmd;
  balance_robot_msgs::data data;

  cmd.r_tau = _cmd.r_tau;
  cmd.l_tau = _cmd.l_tau;
  cmd.p_des = Eigen::vector3fToPoint(_cmd.p_des);
  cmd.v_des = Eigen::vector3fToPoint(_cmd.v_des);
  cmd.rpy_des = Eigen::vector3fToPoint(_cmd.rpy_des);
  cmd.omega_des = Eigen::vector3fToPoint(_cmd.omega_des);
  cmd.header.stamp = ros::Time::now();

  _pub_cmd.publish(cmd);

  data.r_q = _data.r_q;
  data.r_dq = _data.r_dq;
  data.l_q = _data.l_q;
  data.l_dq = _data.l_dq;
  data.quat.w = _data.quat(0);
  data.quat.x = _data.quat(1);
  data.quat.y = _data.quat(2);
  data.quat.z = _data.quat(3);

  data.J_cost = _data.cost.J;
  data.p_act = Eigen::vector3fToPoint(_data.p_act);
  data.vb_act = Eigen::vector3fToPoint(_data.vb_act);
  data.vw_act = Eigen::vector3fToPoint(_data.vw_act);
  data.omega_body = Eigen::vector3fToPoint(_data.omega_body);
  data.omega_world = Eigen::vector3fToPoint(_data.omega_world);
  data.omega_odom = Eigen::vector3fToPoint(_data.omega_odom);
  data.rpy = Eigen::vector3fToPoint(_data.rpy);
  data.p_odom = Eigen::vector3fToPoint(_data.p_odom);
  data.v_odom = Eigen::vector3fToPoint(_data.v_odom);
  data.imu.angular_velocity.x = _data.gyroscope(0);
  data.imu.angular_velocity.y = _data.gyroscope(1);
  data.imu.angular_velocity.z = _data.gyroscope(2);
  data.imu.linear_acceleration.x = _data.acceleration(0);
  data.imu.linear_acceleration.y = _data.acceleration(1);
  data.imu.linear_acceleration.z = _data.acceleration(2);
  data.rpy_odom = Eigen::vector3fToPoint(_data.rpy_odom);

  data.header.stamp = ros::Time::now();

  _pub_data.publish(data);
}

void Robot_Controller::_evalOdom()
{
  _orientationEstimator();

  static float D = 0.068;
  static float h = 2.0 * 0.195 / 3.0;
  static float a = 0.18;
  static float r_q_prev = 0.0;
  static float l_q_prev = 0.0;
  static float yaw_prev = 0.0;

  float Vr = _data.r_dq * D / 2.0;
  float Vl = _data.l_dq * D / 2.0;
  float Vx = (Vl + Vr) / 2.0;
  float wz = Vl / (a + (Vr * a) / (Vl - Vr));
  float rc = Vx / wz;
  float delta_r_q = _data.r_q - r_q_prev;
  float delta_l_q = _data.l_q - l_q_prev;
  float delta_yaw = _data.rpy_odom(2) - yaw_prev;
  float Sx = (D * delta_r_q + D * delta_l_q) / 4.0;
  r_q_prev = _data.r_q;
  l_q_prev = _data.l_q;
  yaw_prev = _data.rpy_odom(2);

  float phi = Sx / rc;
  float P0P1 = 2.0 * rc * sin(phi / 2.0);

  Eigen::Vector3f delta_p(0, 0, 0);

  delta_p(0) = Sx;
  _p_odom += _data.R_odom * delta_p;
  _data.p_odom = _p_odom + ros::pointToVector3f(_p_start);

  _data.omega_odom(0) = _data.gyroscope(0);
  _data.omega_odom(1) = _data.gyroscope(1);
  _data.omega_odom(2) = _data.gyroscope(2);

  _data.omega_odom = _data.R_odom * _data.omega_odom;
  _data.v_odom = _data.R_odom * Eigen::Vector3f(Vx, 0, 0);

  nav_msgs::Odometry odom;

  odom.header.stamp = ros::Time::now();
  odom.header.frame_id = "world";
  odom.child_frame_id = "base_link";

  tf::Quaternion quat;
  quat.setEuler(_data.rpy_odom(2), _data.rpy_odom(1), _data.rpy_odom(0));

  // ground truth
  odom.pose.pose.position = Eigen::vector3fToPoint(_data.p_act);
  odom.pose.pose.orientation.w = _data.quat(0);
  odom.pose.pose.orientation.x = _data.quat(1);
  odom.pose.pose.orientation.y = _data.quat(2);
  odom.pose.pose.orientation.z = _data.quat(3);

  // odom
  // odom.pose.pose.position = Eigen::vector3fToPoint(_data.p_odom);
  // odom.pose.pose.orientation.w = quat.getW();
  // odom.pose.pose.orientation.x = quat.getX();
  // odom.pose.pose.orientation.y = quat.getY();
  // odom.pose.pose.orientation.z = quat.getZ();

  _pub_odom.publish(odom);
}

void Robot_Controller::_writeCmd(float tau, float delta_tau)
{
  if (std::isnan(tau))
  {
    ROS_ERROR("NAN");
    tau = 0.0;
  }

  if (std::isnan(delta_tau))
  {
    ROS_ERROR("NAN");
    delta_tau = 0.0;
  }

  float tau_max = 0.2;

  _cmd.r_tau = tau + delta_tau;
  _cmd.l_tau = tau - delta_tau;

  if (_cmd.r_tau > tau_max)
  {
    _cmd.r_tau = tau_max;
  }
  if (_cmd.r_tau < -tau_max)
  {
    _cmd.r_tau = -tau_max;
  }

  if (_cmd.l_tau > tau_max)
  {
    _cmd.l_tau = tau_max;
  }
  if (_cmd.l_tau < -tau_max)
  {
    _cmd.l_tau = -tau_max;
  }
}

void Robot_Controller::_orientationEstimator()
{
  _complimentaryFilter();
  // _extendedKalmanFilter();
}

void Robot_Controller::_complimentaryFilter()
{
  // float f = 0.95;
  float f = 1.0;

  static Eigen::Vector3f rpy_acc(0, 0, 0);
  static Eigen::Vector3f rpy_gyro(_rpy_start(0), _rpy_start(1), _rpy_start(2));

  rpy_acc(0) = -std::atan2(_data.acceleration(2), _data.acceleration(1)) - 3.1415 / 2.0;         // roll
  rpy_acc(1) = std::atan2(_data.acceleration(2), _data.acceleration(0)) - 3.1415 / 2.0 + 3.1415; // pitch
  rpy_acc(2) = 0.0;                                                                              // yaw

  if (rpy_gyro(2) > PI)
  {
    rpy_gyro(2) -= 2.0 * PI;
  }
  else if (rpy_gyro(2) < -PI)
  {
    rpy_gyro(2) += 2.0 * PI;
  }

  Eigen::Matrix3f Rx;
  Eigen::Matrix3f Ry;
  Eigen::Matrix3f Rz;
  Rx << 1.0, 0.0, 0.0, 0.0, cos(rpy_gyro(0)), -sin(rpy_gyro(0)), 0.0, sin(rpy_gyro(0)), cos(rpy_gyro(0));
  Ry << cos(rpy_gyro(1)), 0.0, sin(rpy_gyro(1)), 0.0, 1.0, 0.0, -sin(rpy_gyro(1)), 0.0, cos(rpy_gyro(1));
  Rz << cos(rpy_gyro(2)), -sin(rpy_gyro(2)), 0.0, sin(rpy_gyro(2)), cos(rpy_gyro(2)), 0.0, 0.0, 0.0, 1.0;
  Eigen::Matrix3f Rb = Rx * Ry;

  // w_w = Rz*Rx*Ry * w_b
  rpy_gyro = rpy_gyro + Rx * Ry * _data.gyroscope * _controller_dt;

  _data.rpy_odom = rpy_acc * (1.0 - f) + f * rpy_gyro;
}

void Robot_Controller::_extendedKalmanFilter()
{
  static Eigen::Vector3f rpy_acc(0, 0, 0);
  // static Eigen::Vector3f rpy_gyro(_rpy_start(0), _rpy_start(1), _rpy_start(2));

  // x_hat_prediction with roll_hat pitch_hat
  static Eigen::Vector2f x_hat(_rpy_start(0), _rpy_start(1));
  static Eigen::Matrix2f P = Eigen::Matrix2f::Zero();
  static Eigen::Matrix2f Q = Eigen::Matrix2f::Zero();
  static Eigen::Matrix3f R = Eigen::Matrix3f::Zero();

  static Eigen::Vector3f gyro_prev = Eigen::Vector3f::Zero();
  static Eigen::Vector3f acc_prev = Eigen::Vector3f(0.0, 0.0, -9.81);

  // float f_acc = 0.7;
  // float f_gyro = 0.7;
  float f_acc = 1.0;
  float f_gyro = 1.0;

  _data.acceleration = (1.0 - f_acc) * acc_prev + f_acc * _data.acceleration;
  _data.gyroscope = (1.0 - f_gyro) * gyro_prev + f_gyro * _data.gyroscope;

  acc_prev = _data.acceleration;
  gyro_prev = _data.gyroscope;

  // no noise
  P(0, 0) = 1e-4;
  P(1, 1) = 1e-4;
  Q(0, 0) = 1e-2;
  Q(1, 1) = 1e-2;
  R(0, 0) = 1e4;
  R(1, 1) = 1e4;
  R(2, 2) = 1;

  // noise
  // P(0, 0) = 1e-2;
  // P(1, 1) = 1e-2;
  // Q(0, 0) = 1e-4;
  // Q(1, 1) = 1e-4;
  // R(0, 0) = 1e5;
  // R(1, 1) = 1e5;
  // R(2, 2) = 1;

  float roll = x_hat(0);
  float pitch = x_hat(1);
  float p = _data.gyroscope(0);
  float q = _data.gyroscope(1);
  float r = _data.gyroscope(2);

  Eigen::Matrix3f Rx;
  Eigen::Matrix3f Ry;
  Eigen::Matrix3f Rz;
  Rx << 1.0, 0.0, 0.0, 0.0, cos(x_hat(0)), -sin(x_hat(0)), 0.0, sin(x_hat(0)), cos(x_hat(0));
  Ry << cos(x_hat(1)), 0.0, sin(x_hat(1)), 0.0, 1.0, 0.0, -sin(x_hat(1)), 0.0, cos(x_hat(1));
  // Rz << cos(rpy_gyro(2)), -sin(rpy_gyro(2)), 0.0, sin(rpy_gyro(2)), cos(rpy_gyro(2)), 0.0, 0.0, 0.0, 1.0;
  // Rx << 1.0, 0.0, 0.0, 0.0, cos(_data.rpy_odom(0)), -sin(_data.rpy_odom(0)), 0.0, sin(_data.rpy_odom(0)), cos(_data.rpy_odom(0));
  // Ry << cos(_data.rpy_odom(1)), 0.0, sin(_data.rpy_odom(1)), 0.0, 1.0, 0.0, -sin(_data.rpy_odom(1)), 0.0, cos(_data.rpy_odom(1));
  Eigen::Matrix3f Rb = Rx * Ry;

  Eigen::Matrix2f A;
  A(0, 0) = -cos(pitch) * r - p * sin(pitch);
  A(0, 1) = 0.0;
  A(1, 0) = r * sin(pitch) * sin(roll) - p * cos(pitch) * sin(roll);
  A(1, 1) = -q * sin(roll) - cos(pitch) * r * cos(roll) - p * cos(roll) * sin(pitch);

  static float g = 9.81;
  Eigen::Matrix<float, 3, 2> C;
  C(0, 0) = 0.0;
  C(0, 1) = g * cos(pitch);
  C(1, 0) = -g * cos(pitch) * cos(roll);
  C(1, 1) = g * sin(pitch) * sin(roll);
  C(2, 0) = g * cos(pitch) * sin(roll);
  C(2, 1) = g * cos(roll) * sin(pitch);

  Eigen::Matrix<float, 2, 3> K = Eigen::Matrix<float, 2, 3>::Zero();
  Eigen::Vector3f y(_data.acceleration(0), _data.acceleration(1), _data.acceleration(2));
  Eigen::Vector3f h;
  h(0) = sin(pitch) * g;
  h(1) = -cos(pitch) * sin(roll) * g;
  h(2) = -cos(pitch) * cos(roll) * g;

  x_hat = x_hat + (Rx * Ry * _data.gyroscope * _controller_dt).block<2, 1>(0, 0);
  P = P + _controller_dt * (A * P + P * A.transpose() + Q);
  // P = A * P * A.transpose() + Q;
  K = P * C.transpose() * (C * P * C.transpose() + R).inverse();
  // x_hat = x_hat + K * (y - h);
  P = (Eigen::Matrix2f::Identity() - K * C) * P;

  // cout << "rpy_odom: " << _data.rpy_odom.block<2, 1>(0, 0) << endl;
  // cout << "x_hat: " << x_hat << endl;
  // cout << "P: " << P << endl;
  // cout << "K: " << K << endl;
  // cout << "y-h: " << y - h << endl;
  // cout << "y: " << y << " h: " << h << endl;

  _data.rpy_odom(0) = x_hat(0);
  _data.rpy_odom(1) = x_hat(1);

  Rx << 1.0, 0.0, 0.0, 0.0, cos(_data.rpy_odom(0)), -sin(_data.rpy_odom(0)), 0.0, sin(_data.rpy_odom(0)), cos(_data.rpy_odom(0));
  Ry << cos(_data.rpy_odom(1)), 0.0, sin(_data.rpy_odom(1)), 0.0, 1.0, 0.0, -sin(_data.rpy_odom(1)), 0.0, cos(_data.rpy_odom(1));
  Rz << cos(_data.rpy_odom(2)), -sin(_data.rpy_odom(2)), 0.0, sin(_data.rpy_odom(2)), cos(_data.rpy_odom(2)), 0.0, 0.0, 0.0, 1.0;
  _data.R_odom = Rz * Rx * Ry;
}

void Robot_Controller::_control1()
{
  // for odom without noise
  float Kp = 8e3;
  // float Kd = 1.1e1;
  float P = 1.5e4;
  float D = 0.5e1;
  // float P_vq = 5e-1;

  // no D
  float Kd = 7e1;
  float P_vq = 5e-1;

  // for odom with noise
  // float Kp = 7e3;
  // float Kd = 6e1;
  // float P = 1e3;
  // float D = 2e1;
  // float P_vq = 3e-1;

  float q = _data.rpy(1);
  float dq = _data.omega_body(1);
  // float q = _data.rpy_odom(1);
  // float dq = _data.gyroscope(1);
  // float x_act = _data.p_act(0);
  float v_act = _data.vb_act(0);

  static double roll_des = 0.0;
  static double pitch_des = 0.0;
  static float yaw_des = 0.0;
  static float yaw_integral = 0.0;

  pitch_des = P_vq * (_cmd.v_des(0) - v_act);

  if ((M_PI - abs(yaw_des)) <= 0.1)
  {
    yaw_des = _data.rpy(2) + _cmd.omega_des(2) * _controller_dt;
  }
  else
  {
    yaw_des += _cmd.omega_des(2) * _controller_dt;
  }

  _cmd.rpy_des(2) = yaw_des;

  yaw_integral += yaw_des - _data.rpy(2);

  if (yaw_integral > 0.2)
  {
    yaw_integral = 0.2;
  }
  if (yaw_integral < -0.2)
  {
    yaw_integral = -0.2;
  }

  if (abs(_cmd.v_des(0)) > 1e-4)
  {
    _cmd.p_des(0) = _referenceTrajectory.poses.at(0).pose.position.x;
    _cmd.p_des(1) = _referenceTrajectory.poses.at(0).pose.position.y;
    _cmd.p_des(2) = _referenceTrajectory.poses.at(0).pose.position.z;
    _cmd.p_des = _data.R * _cmd.p_des + _data.p_act - _data.R * Eigen::Vector3f(sin(_data.rpy(1)) * 0.195 / 2.0, 0, 0);
  }

  Eigen::Vector2f p_err = _cmd.p_des.block<2, 1>(0, 0) - _data.p_act.block<2, 1>(0, 0);
  float angle = atan2(p_err(1), p_err(0)) - _data.rpy(2);
  float sign = 1.0;

  if (abs(angle) > PI / 2.0)
  {
    sign = -1.0;
  }

  _cmd.v_des(1) = 0.0;
  _cmd.v_des(2) = 0.0;

  _cmd.rpy_des(0) = 0.0;
  _cmd.rpy_des(1) = 0.0 + pitch_des;

  _cmd.omega_des(0) = 0.0;
  _cmd.omega_des(1) = 0.0;

  // nonlinear from MCG
  float ddq = 0.0;
  float tau = 0.0;
  float delta_tau = 0.0;
  // ddq = Kp * (_cmd.rpy_des(1) - q) + Kd * (_cmd.omega_des(1) - dq) + P * sign * (_cmd.p_des.block<2, 1>(0, 0) - _data.p_act.block<2, 1>(0, 0)).norm();
  // ddq *= -1.0;
  // tau = (1.0 * ((0.186 * (0.0198 * ddq + 0.613 * sin(q))) / cos(q) - 0.00212 * ddq * cos(q) + 0.00212 * sin(q) * dq * dq)) / (0.371 / cos(q) + 2.0);
  // delta_tau = 1e-1 * (_cmd.rpy_des(2) - _data.rpy(2)) + 1e-1 * (_cmd.omega_des(2) - _data.omega_body(2));

  static const uint8_t num_vars = 4;
  Eigen::MatrixXd H_LQR;
  Eigen::MatrixXd A_LQR;
  Eigen::MatrixXd B_LQR;
  Eigen::MatrixXd Q_LQR;
  Eigen::MatrixXd R_LQR;
  Eigen::MatrixXd P_LQR;

  H_LQR.resize(2 * num_vars, 2 * num_vars);
  A_LQR.resize(num_vars, num_vars);
  B_LQR.resize(num_vars, 1);
  R_LQR.resize(1, 1);
  Q_LQR.resize(num_vars, num_vars);

  Q_LQR.setZero();

  Q_LQR(0, 0) = 1e0; // p
  Q_LQR(1, 1) = 2e6; // q
  Q_LQR(2, 2) = 1e3; // dq
  Q_LQR(3, 3) = 1e2; // v

  R_LQR(0) = 4e5;

  A_LQR.block<1, 4>(0, 0) << 0.0, 0.0, 0.0, -0.034;
  A_LQR.block<1, 4>(1, 0) << 0.0, 0.0, 1.0, 0.0;
  A_LQR.block<1, 4>(2, 0) << 0.0, -152.7571, 0.0, 0.0;
  A_LQR.block<1, 4>(3, 0) << 0.0, 405.0554, 0.0, 0.0;

  B_LQR << 0, 0, -1.6069e3, 6.4648e3;

  // Compute the Hamiltonian and its eigenvalues/vectors
  H_LQR.block<num_vars, num_vars>(0, 0) << A_LQR;
  H_LQR.block<num_vars, num_vars>(0, num_vars) << -B_LQR * R_LQR.inverse() * B_LQR.transpose();
  H_LQR.block<num_vars, num_vars>(num_vars, 0) << -Q_LQR;
  H_LQR.block<num_vars, num_vars>(num_vars, num_vars) << -A_LQR.transpose();
  Eigen::EigenSolver<Eigen::MatrixXd> es(H_LQR);

  // Create a 2nxn matrix U=[U11;U21] containing the eigenvectors of the stable eigenvalues
  Eigen::MatrixXcd U;
  U.setZero(2 * num_vars, num_vars);
  Eigen::MatrixXcd U11;
  U11.setZero(num_vars, num_vars);
  Eigen::MatrixXcd U21;
  U21.setZero(num_vars, num_vars);
  Eigen::MatrixXcd P_complex;
  P_complex.setZero(num_vars, num_vars);
  std::complex<double> lambda;

  // U contains eigenvectors corresponding to stable eigenvalues of the Hamiltonian
  int j = 0;
  for (int i = 0; i < 2 * num_vars; i++)
  {
    lambda = es.eigenvalues()[i];

    if (lambda.real() < 0)
    {
      U.block<2 * num_vars, 1>(0, j) << es.eigenvectors().col(i);
      j = j + 1;
    }
  }

  // Compute P based on U11*P = -U21;
  U11 = U.block<num_vars, num_vars>(0, 0);
  U21 = U.block<num_vars, num_vars>(num_vars, 0);
  P_complex = U21 * U11.inverse();
  P_LQR = P_complex.real();
  // cout << "P: " << P_LQR << endl;
  // cout << "K: " << R_LQR.inverse() * B_LQR.transpose() * P_LQR << endl;

  // LQR
  Eigen::Matrix<float, 1, 4> Kr = Eigen::Matrix<float, 1, 4>::Zero();
  Eigen::Matrix<float, 4, 1> x = Eigen::Matrix<float, 4, 1>::Zero();
  // Kr << -99999.9986, -3160122.9412, 16780.3545, 14232.5121;
  // Kr << -10, -216.7339, -10.8018, 10.0351;
  Kr = (R_LQR.inverse() * B_LQR.transpose() * P_LQR).cast<float>();
  x(0) = sign * (_cmd.p_des.block<2, 1>(0, 0) - _data.p_act.block<2, 1>(0, 0)).norm();
  x(1) = 0.0 - q;
  x(2) = 0.0 - dq;
  x(3) = _cmd.v_des(0) - v_act;
  tau = Kr * x;
  delta_tau = 1e-1 * (_cmd.rpy_des(2) - _data.rpy(2)) + 1e-1 * (_cmd.omega_des(2) - _data.omega_body(2));
  // delta_tau = 0.0;

  _writeCmd(tau, delta_tau);
}

void Robot_Controller::_lqr1()
{
}

void Robot_Controller::_lqrV2C()
{
  // for odom without noise
  float Kp = 8e3;
  float P = 1.5e4;
  float D = 0.5e1;
  float Kd = 7e1;
  float P_vq = 5e-1;

  float q = _data.rpy(1);
  float dq = _data.omega_body(1);
  float v_act = _data.vb_act(0);

  static double roll_des = 0.0;
  static double pitch_des = 0.0;
  static float yaw_des = 0.0;
  static float yaw_integral = 0.0;
  float a = 0;
  float tf = 1;
  static float ddx_des = 0;

  if ((M_PI - abs(yaw_des)) <= 0.1)
  {
    yaw_des = _data.rpy(2) + _cmd.omega_des(2) * _controller_dt;
  }
  else
  {
    yaw_des += _cmd.omega_des(2) * _controller_dt;
  }

  _cmd.rpy_des(2) = yaw_des;

  yaw_integral += yaw_des - _data.rpy(2);

  if (yaw_integral > 0.2)
  {
    yaw_integral = 0.2;
  }
  if (yaw_integral < -0.2)
  {
    yaw_integral = -0.2;
  }

  if (abs(_cmd.v_cmd(0)) > 1e-4)
  {
    _cmd.p_des(0) = _referenceTrajectory.poses.at(0).pose.position.x;
    _cmd.p_des(1) = _referenceTrajectory.poses.at(0).pose.position.y;
    _cmd.p_des(2) = _referenceTrajectory.poses.at(0).pose.position.z;
    _cmd.p_des = _data.R * _cmd.p_des + _data.p_act - _data.R * Eigen::Vector3f(sin(_data.rpy(1)) * 0.195 / 2.0, 0, 0);
  }
  // else
  // {
  //   // _cmd.p_des = _data.p_act - _data.R * Eigen::Vector3f(sin(_data.rpy(1)) * 0.195 / 2.0, 0, 0);
  //   _cmd.p_des = _data.p_act;
  // }

  Eigen::Vector2f p_err = _cmd.p_des.block<2, 1>(0, 0) - _data.p_act.block<2, 1>(0, 0);
  float angle = atan2(p_err(1), p_err(0)) - _data.rpy(2);
  float sign = 1.0;

  if (abs(angle) > PI / 2.0)
  {
    sign = -1.0;
  }

  if (abs(_cmd.v_cmd(0)) < 1e-4)
  {
    _cmd.v_des(0) = 4.0 * sign * p_err.norm();
  }
  else
  {
    _cmd.v_des(0) = _cmd.v_cmd(0);
  }

  static float v_cmd_prev = 0.0;
  static float v_start = 0;
  // cout << round(_cmd.v_cmd(0) * 10.0) / 10.0 << endl;

  // _cmd.v_cmd(0) = round(_cmd.v_cmd(0) * 10.0) / 10.0; // 0.1 step

  if (abs(_cmd.v_cmd(0)) > 0.09)
  {
    v_cmd_prev = _cmd.v_cmd(0);
    t_start = ros::Time::now();
    v_start = _data.vb_act(0);
  }

  // a = 4.0 * _cmd.v_des(0) / (tf * tf);
  // a = 4.0 * (_cmd.v_des(0) - _data.vb_act(0)) / (tf * tf);
  a = 4.0 * (_cmd.v_des(0) - v_start) / (tf * tf);

  double t = _getTime();

  // cout << t << endl;

  if (t < tf / 2.0)
  {
    ddx_des += a * _controller_dt;
  }
  else
  {
    ddx_des -= a * _controller_dt;
  }

  if (t > tf)
  {
    ddx_des = 0.0;
  }

  pitch_des = asin(ddx_des * (2.5 * _params.m * _params.r - 2.0 * _params.Jw / _params.r) / (_params.M * 9.81 * _params.h));

  _cmd.v_des(1) = 0.0;
  _cmd.v_des(2) = 0.0;

  _cmd.rpy_des(0) = 0.0;
  _cmd.rpy_des(1) = 0.0 + pitch_des;
  // cout << "pitch des: " << pitch_des << endl;

  _cmd.omega_des(0) = 0.0;
  _cmd.omega_des(1) = 0.0;

  // nonlinear from MCG
  float ddq = 0.0;
  float tau = 0.0;
  float delta_tau = 0.0;

  static const uint8_t num_vars = 4;
  Eigen::MatrixXd H_LQR;
  Eigen::MatrixXd A_LQR;
  Eigen::MatrixXd B_LQR;
  Eigen::MatrixXd Q_LQR;
  Eigen::MatrixXd R_LQR;
  Eigen::MatrixXd P_LQR;

  H_LQR.resize(2 * num_vars, 2 * num_vars);
  A_LQR.resize(num_vars, num_vars);
  B_LQR.resize(num_vars, 1);
  R_LQR.resize(1, 1);
  Q_LQR.resize(num_vars, num_vars);

  Q_LQR.setZero();

  float Q_q = 0;
  float Q_dq = 0;
  float Q_x = 0;
  float Q_dx = 0;
  float R = 0;

  ros::readRosParam("/static_params/lqr2/Q_q", Q_q);
  ros::readRosParam("/static_params/lqr2/Q_dq", Q_dq);
  ros::readRosParam("/static_params/lqr2/Q_x", Q_x);
  ros::readRosParam("/static_params/lqr2/Q_dx", Q_dx);
  ros::readRosParam("/static_params/lqr2/R", R);

  // Q_LQR(0, 0) = 1.5e1;
  // Q_LQR(1, 1) = 3e0;
  // Q_LQR(2, 2) = 1e-1;
  // Q_LQR(3, 3) = 1.5e1;
  // R_LQR(0) = 5e0;

  Q_LQR(0, 0) = Q_q;
  Q_LQR(1, 1) = Q_dq;
  Q_LQR(2, 2) = Q_x;
  Q_LQR(3, 3) = Q_dx;
  R_LQR(0) = R;

  // V2
  A_LQR.block<1, 4>(0, 0) << 0, 1, 0, 0;
  A_LQR.block<1, 4>(1, 0) << -301.8462, 0, 0, 0;
  A_LQR.block<1, 4>(2, 0) << 0, 0, 0, 1;
  A_LQR.block<1, 4>(3, 0) << 0, 0, 0, 0;

  B_LQR << 0, 2, 0, 141.1313;

  // discrete form
  // A_LQR = A_LQR * _controller_dt + Eigen::Matrix<double, 4, 4>::Identity();
  // B_LQR = B_LQR * _controller_dt;

  // Compute the Hamiltonian and its eigenvalues/vectors
  H_LQR.block<num_vars, num_vars>(0, 0) << A_LQR;
  H_LQR.block<num_vars, num_vars>(0, num_vars) << -B_LQR * R_LQR.inverse() * B_LQR.transpose();
  H_LQR.block<num_vars, num_vars>(num_vars, 0) << -Q_LQR;
  H_LQR.block<num_vars, num_vars>(num_vars, num_vars) << -A_LQR.transpose();
  Eigen::EigenSolver<Eigen::MatrixXd> es(H_LQR);

  // Create a 2nxn matrix U=[U11;U21] containing the eigenvectors of the stable eigenvalues
  Eigen::MatrixXcd U;
  Eigen::MatrixXcd U11;
  Eigen::MatrixXcd U21;
  Eigen::MatrixXcd P_complex;

  U.setZero(2 * num_vars, num_vars);
  U11.setZero(num_vars, num_vars);
  U21.setZero(num_vars, num_vars);
  P_complex.setZero(num_vars, num_vars);
  std::complex<double> lambda;

  // U contains eigenvectors corresponding to stable eigenvalues of the Hamiltonian
  int j = 0;
  for (int i = 0; i < 2 * num_vars; i++)
  {
    lambda = es.eigenvalues()[i];

    if (lambda.real() < 0)
    {
      U.block<2 * num_vars, 1>(0, j) << es.eigenvectors().col(i);
      j = j + 1;
    }
  }

  // Compute P based on U11*P = -U21;
  U11 = U.block<num_vars, num_vars>(0, 0);
  U21 = U.block<num_vars, num_vars>(num_vars, 0);
  P_complex = U21 * U11.inverse();
  P_LQR = P_complex.real();

  // cout << "P: " << P_LQR << endl;

  // LQR
  Eigen::Matrix<float, 1, 4> Kr = Eigen::Matrix<float, 1, 4>::Zero();
  Eigen::Matrix<float, 4, 1> x = Eigen::Matrix<float, 4, 1>::Zero();
  Kr = -(R_LQR.inverse() * B_LQR.transpose() * P_LQR).cast<float>();

  // cout << "Kr: " << Kr << endl;
  ROS_INFO_STREAM_ONCE(Kr);

  x(0) = 0.0 - q;
  // x(0) = _cmd.rpy_des(1) - q;
  x(1) = 0.0 - dq;
  x(2) = sign * (_cmd.p_des.block<2, 1>(0, 0) - _data.p_act.block<2, 1>(0, 0)).norm();
  x(3) = _cmd.v_des(0) - v_act;
  // x(2) = 0.0;
  // x(3) = 0.0;
  tau = Kr * x;
  delta_tau = 1e-1 * (_cmd.rpy_des(2) - _data.rpy(2)) + 1e-1 * (_cmd.omega_des(2) - _data.omega_body(2));
  // delta_tau = 0;

  _writeCmd(tau, delta_tau);

  _evalCostFunction(Q_LQR.cast<float>(), R_LQR(0), x, tau);
}

void Robot_Controller::_lqrV2H()
{
  // for odom without noise
  float Kp = 8e3;
  float P = 1.5e4;
  float D = 0.5e1;
  float Kd = 7e1;
  float P_vq = 5e-1;

  float q = _data.rpy(1);
  float dq = _data.omega_body(1);
  float v_act = _data.vb_act(0);

  static double roll_des = 0.0;
  static double pitch_des = 0.0;
  static float yaw_des = 0.0;
  static float yaw_integral = 0.0;
  float a = 0;
  float tf = 1;
  static float ddx_des = 0;

  static const float m = 0.0963;
  static const float r = 0.068 / 2.0;
  static const float Jw = 0.024 * r * r;
  static const float M = 0.5444;
  static const float h = 0.195 / 2.0;

  if ((M_PI - abs(yaw_des)) <= 0.1)
  {
    yaw_des = _data.rpy(2) + _cmd.omega_des(2) * _controller_dt;
  }
  else
  {
    yaw_des += _cmd.omega_des(2) * _controller_dt;
  }

  _cmd.rpy_des(2) = yaw_des;

  yaw_integral += yaw_des - _data.rpy(2);

  if (yaw_integral > 0.2)
  {
    yaw_integral = 0.2;
  }
  if (yaw_integral < -0.2)
  {
    yaw_integral = -0.2;
  }

  if (abs(_cmd.v_cmd(0)) > 1e-4)
  {
    _cmd.p_des(0) = _referenceTrajectory.poses.at(0).pose.position.x;
    _cmd.p_des(1) = _referenceTrajectory.poses.at(0).pose.position.y;
    _cmd.p_des(2) = _referenceTrajectory.poses.at(0).pose.position.z;
    _cmd.p_des = _data.R * _cmd.p_des + _data.p_act - _data.R * Eigen::Vector3f(sin(_data.rpy(1)) * 0.195 / 2.0, 0, 0);
  }

  Eigen::Vector2f p_err = _cmd.p_des.block<2, 1>(0, 0) - _data.p_act.block<2, 1>(0, 0);
  float angle = atan2(p_err(1), p_err(0)) - _data.rpy(2);
  float sign = 1.0;

  if (abs(angle) > PI / 2.0)
  {
    sign = -1.0;
  }

  if (abs(_cmd.v_cmd(0)) < 1e-4)
  {
    _cmd.v_des(0) = 4.0 * sign * p_err.norm();
  }
  else
  {
    _cmd.v_des(0) = _cmd.v_cmd(0);
  }

  static float v_cmd_prev = 0.0;
  static float v_start = 0;

  if (abs(_cmd.v_cmd(0)) > 0.09)
  {
    v_cmd_prev = _cmd.v_cmd(0);
    t_start = ros::Time::now();
    v_start = _data.vb_act(0);
  }

  a = 4.0 * (_cmd.v_des(0) - v_start) / (tf * tf);

  double t = _getTime();

  if (t < tf / 2.0)
  {
    ddx_des += a * _controller_dt;
  }
  else
  {
    ddx_des -= a * _controller_dt;
  }

  if (t > tf)
  {
    ddx_des = 0.0;
  }

  pitch_des = asin(ddx_des * (2.5 * m * r - 2.0 * Jw / r) / (M * 9.81 * h));

  _cmd.v_des(1) = 0.0;
  _cmd.v_des(2) = 0.0;

  _cmd.rpy_des(0) = 0.0;
  _cmd.rpy_des(1) = 0.0 + pitch_des;

  _cmd.omega_des(0) = 0.0;
  _cmd.omega_des(1) = 0.0;

  // nonlinear from MCG
  float ddq = 0.0;
  float tau = 0.0;
  float delta_tau = 0.0;

  static const uint8_t num_vars = 4;
  Eigen::MatrixXd H_LQR;
  Eigen::MatrixXd A_LQR;
  Eigen::MatrixXd B_LQR;
  Eigen::MatrixXd Q_LQR;
  Eigen::MatrixXd R_LQR;
  Eigen::MatrixXd P_LQR;

  H_LQR.resize(2 * num_vars, 2 * num_vars);
  A_LQR.resize(num_vars, num_vars);
  B_LQR.resize(num_vars, 1);
  R_LQR.resize(1, 1);
  Q_LQR.resize(num_vars, num_vars);

  Q_LQR.setZero();

  float Q_q = 0;
  float Q_dq = 0;
  float Q_x = 0;
  float Q_dx = 0;
  float R = 0;

  ros::readRosParam("/static_params/lqr2/Q_q", Q_q);
  ros::readRosParam("/static_params/lqr2/Q_dq", Q_dq);
  ros::readRosParam("/static_params/lqr2/Q_x", Q_x);
  ros::readRosParam("/static_params/lqr2/Q_dx", Q_dx);
  ros::readRosParam("/static_params/lqr2/R", R);

  Q_LQR(0, 0) = Q_q;
  Q_LQR(1, 1) = Q_dq;
  Q_LQR(2, 2) = Q_x;
  Q_LQR(3, 3) = Q_dx;
  R_LQR(0) = R;

  // V2
  A_LQR.block<1, 4>(0, 0) << 0, 1, 0, 0;
  A_LQR.block<1, 4>(1, 0) << -301.8462, 0, 0, 0;
  A_LQR.block<1, 4>(2, 0) << 0, 0, 0, 1;
  A_LQR.block<1, 4>(3, 0) << 0, 0, 0, 0;

  B_LQR << 0, 2, 0, 141.1313;

  // Compute the Hamiltonian and its eigenvalues/vectors
  H_LQR.block<num_vars, num_vars>(0, 0) << A_LQR;
  H_LQR.block<num_vars, num_vars>(0, num_vars) << -B_LQR * R_LQR.inverse() * B_LQR.transpose();
  H_LQR.block<num_vars, num_vars>(num_vars, 0) << -Q_LQR;
  H_LQR.block<num_vars, num_vars>(num_vars, num_vars) << -A_LQR.transpose();
  Eigen::EigenSolver<Eigen::MatrixXd> es(H_LQR);

  // Create a 2nxn matrix U=[U11;U21] containing the eigenvectors of the stable eigenvalues
  Eigen::MatrixXcd U;
  Eigen::MatrixXcd U11;
  Eigen::MatrixXcd U21;
  Eigen::MatrixXcd P_complex;

  U.setZero(2 * num_vars, num_vars);
  U11.setZero(num_vars, num_vars);
  U21.setZero(num_vars, num_vars);
  P_complex.setZero(num_vars, num_vars);
  std::complex<double> lambda;

  // U contains eigenvectors corresponding to stable eigenvalues of the Hamiltonian
  int j = 0;
  for (int i = 0; i < 2 * num_vars; i++)
  {
    lambda = es.eigenvalues()[i];

    if (lambda.real() < 0)
    {
      U.block<2 * num_vars, 1>(0, j) << es.eigenvectors().col(i);
      j = j + 1;
    }
  }

  // Compute P based on U11*P = -U21;
  U11 = U.block<num_vars, num_vars>(0, 0);
  U21 = U.block<num_vars, num_vars>(num_vars, 0);
  P_complex = U21 * U11.inverse();
  P_LQR = P_complex.real();

  // cout << "P: " << P_LQR << endl;

  // LQR
  Eigen::Matrix<float, 1, 4> Kr = Eigen::Matrix<float, 1, 4>::Zero();
  Eigen::Matrix<float, 4, 1> x = Eigen::Matrix<float, 4, 1>::Zero();
  Kr = -(R_LQR.inverse() * B_LQR.transpose() * P_LQR).cast<float>();
  x(0) = 0.0 - q;
  x(1) = 0.0 - dq;
  x(2) = sign * (_cmd.p_des.block<2, 1>(0, 0) - _data.p_act.block<2, 1>(0, 0)).norm();
  x(3) = _cmd.v_des(0) - v_act;
  // x(2) = 0.0;
  // x(3) = 0.0;
  tau = Kr * x;
  delta_tau = 1e-1 * (_cmd.rpy_des(2) - _data.rpy(2)) + 1e-1 * (_cmd.omega_des(2) - _data.omega_body(2));

  _writeCmd(tau, delta_tau);

  _evalCostFunction(Q_LQR.cast<float>(), R_LQR(0), x, tau);
}

void Robot_Controller::_lqr_matlab()
{
  float q = _data.rpy(1);
  float dq = _data.omega_body(1);
  float v_act = _data.vb_act(0);

  static double roll_des = 0.0;
  static double pitch_des = 0.0;
  static float yaw_des = 0.0;
  static float yaw_integral = 0.0;

  // eval yaw desired based on omega des via integration except near 180 deg
  if ((M_PI - abs(yaw_des)) <= 0.1)
  {
    yaw_des = _data.rpy(2) + _cmd.omega_des(2) * _controller_dt;
  }
  else
  {
    yaw_des += _cmd.omega_des(2) * _controller_dt;
  }

  _cmd.rpy_des(2) = yaw_des;

  // if desired vel more than 0, take first pose from reference trajectory
  if (abs(_cmd.v_cmd(0)) > 1e-4)
  {
    _cmd.p_des(0) = _referenceTrajectory.poses.at(0).pose.position.x;
    _cmd.p_des(1) = _referenceTrajectory.poses.at(0).pose.position.y;
    _cmd.p_des(2) = _referenceTrajectory.poses.at(0).pose.position.z;
    _cmd.p_des = _data.R * _cmd.p_des + _data.p_act - _data.R * Eigen::Vector3f(sin(_data.rpy(1)) * _params.h, 0, 0);
  }

  Eigen::Vector2f p_err = _cmd.p_des.block<2, 1>(0, 0) - _data.p_act.block<2, 1>(0, 0);
  float angle = atan2(p_err(1), p_err(0)) - _data.rpy(2);
  float sign = 1.0;

  if (abs(angle) > PI / 2.0)
  {
    sign = -1.0;
  }

  // if desired vel == 0 than slowly get to desired pose
  if (abs(_cmd.v_cmd(0)) < 1e-4)
  {
    _cmd.v_des(0) = 4.0 * sign * p_err.norm();
  }
  else
  {
    _cmd.v_des(0) = _cmd.v_cmd(0);
  }

  _cmd.v_des(1) = 0.0;
  _cmd.v_des(2) = 0.0;

  _cmd.rpy_des(0) = 0.0;
  _cmd.rpy_des(1) = 0.0;

  _cmd.omega_des(0) = 0.0;
  _cmd.omega_des(1) = 0.0;

  float tau = 0.0;
  float delta_tau = 0.0;

  static const uint8_t num_vars = 4;
  Eigen::MatrixXd A_LQR;
  Eigen::MatrixXd B_LQR;
  Eigen::Matrix<float, 1, 4> Kr = Eigen::Matrix<float, 1, 4>::Zero();
  Eigen::Matrix<float, 4, 1> x = Eigen::Matrix<float, 4, 1>::Zero();

  A_LQR.resize(num_vars, num_vars);
  B_LQR.resize(num_vars, 1);

  // V2 true discrete form
  A_LQR << 0.98495, 0.0099498, 0, 0, -3.0033, 0.98495, 0, 0, 0, 0, 1, 0.01, 0, 0, 0, 1;
  B_LQR << 9.9749e-05, 0.0199, 0.0070566, 1.4113;
  Kr << 1.6296, 0.013251, 0.66855, 0.60592;

  x(0) = 0.0 - q;
  x(1) = 0.0 - dq;
  x(2) = sign * (_cmd.p_des.block<2, 1>(0, 0) - _data.p_act.block<2, 1>(0, 0)).norm();
  x(3) = _cmd.v_des(0) - v_act;
  // x(2) = 0.0;
  // x(3) = 0.0;
  tau = -Kr * x;
  delta_tau = 1e-1 * (_cmd.rpy_des(2) - _data.rpy(2)) + 1e-1 * (_cmd.omega_des(2) - _data.omega_body(2));

  _writeCmd(tau, delta_tau);

  // _evalCostFunction(Q_LQR.cast<float>(), R_LQR(0), x, tau);
}

void Robot_Controller::_matrixToReal(qpOASES::real_t* dst, Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> src, uint16_t rows, uint16_t cols)
{
  uint32_t element = 0;

  for (uint16_t r = 0; r < rows; r++)
  {
    for (uint16_t c = 0; c < cols; c++)
    {
      dst[element] = src(r, c);

      element++;
    }
  }
}

void Robot_Controller::_mpc()
{
  float q = _data.rpy(1);
  float dq = _data.omega_body(1);
  float v_act = _data.vb_act(0);
  float ddq = 0.0;
  float tau = 0.0;
  float delta_tau = 0.0;
  static float yaw_des = 0.0;
  static float yaw_integral = 0.0;
  static const float m = 0.0963;
  static const float r = 0.068 / 2.0;
  static const float Jw = 0.024 * r * r;
  static const float M = 0.5444;
  static const float h = 0.195 / 2.0;
  static double pitch_des = 0.0;
  float a = 0;
  float tf = 1;
  static float ddx_des = 0;

  a = 4.0 * (_cmd.v_des(0) - _data.vb_act(0)) / (tf * tf);

  ddx_des += a * _controller_dt;

  pitch_des = asin(ddx_des * (2.5 * m * r - 2.0 * Jw / r) / (M * 9.81 * h));

  // _cmd.rpy_des(1) = pitch_des;
  _cmd.rpy_des(1) = 0;

  if (abs(_cmd.v_cmd(0)) > 1e-2)
  {
    _cmd.p_des(0) = _referenceTrajectory.poses.at(0).pose.position.x;
    _cmd.p_des(1) = _referenceTrajectory.poses.at(0).pose.position.y;
    _cmd.p_des(2) = _referenceTrajectory.poses.at(0).pose.position.z;
    _cmd.p_des = _data.R * _cmd.p_des + _data.p_act - _data.R * Eigen::Vector3f(sin(_data.rpy(1)) * 0.195 / 2.0, 0, 0);
  }

  Eigen::Vector2f p_err = _cmd.p_des.block<2, 1>(0, 0) - _data.p_act.block<2, 1>(0, 0);
  float angle = atan2(p_err(1), p_err(0)) - _data.rpy(2);
  float sign = 1.0;

  if (abs(angle) > PI / 2.0)
  {
    sign = -1.0;
  }

  _cmd.v_des(0) = _cmd.v_cmd(0);

  if ((M_PI - abs(yaw_des)) <= 0.1)
  {
    yaw_des = _data.rpy(2) + _cmd.omega_des(2) * _controller_dt;
  }
  else
  {
    yaw_des += _cmd.omega_des(2) * _controller_dt;
  }

  _cmd.rpy_des(2) = yaw_des;

  yaw_integral += yaw_des - _data.rpy(2);

  if (yaw_integral > 0.2)
  {
    yaw_integral = 0.2;
  }
  if (yaw_integral < -0.2)
  {
    yaw_integral = -0.2;
  }

  static const uint16_t num_u = 1;    // number of control
  static const uint16_t num_vars = 4; // number of variables
  static const uint16_t num_cons = 1; // number of constraints
  static const uint16_t horizon = 10; // horizon

  Eigen::Matrix<double, num_vars, 1> L;
  Eigen::Matrix<double, num_vars * horizon, num_vars * horizon> L_qp;
  Eigen::Matrix<double, num_u, 1> K;
  Eigen::Matrix<double, num_u * horizon, num_u * horizon> K_qp;
  Eigen::Matrix<double, num_vars, num_vars> A;
  Eigen::Matrix<double, num_vars, num_u> B;
  Eigen::Matrix<double, num_vars, num_vars> Ad;
  Eigen::Matrix<double, num_vars, num_u> Bd;
  Eigen::Matrix<double, num_vars * horizon, num_vars> Ad_qp;
  Eigen::Matrix<double, num_vars * horizon, num_u * horizon> Bd_qp;
  Eigen::Matrix<double, num_u * horizon, num_u * horizon> H_qp;
  Eigen::Matrix<double, num_u * horizon, 1> g_qp;
  Eigen::Matrix<double, num_vars, 1> x;
  Eigen::Matrix<double, num_vars * horizon, 1> x_d;

  L.setZero();
  L_qp.setZero();
  K.setZero();
  K_qp.setZero();
  A.setZero();
  B.setZero();
  Ad.setZero();
  Bd.setZero();
  Ad_qp.setZero();
  Bd_qp.setZero();
  x.setZero();
  x_d.setZero();
  H_qp.setZero();
  g_qp.setZero();

  double Q_q = 0;
  double Q_dq = 0;
  double Q_x = 0;
  double Q_dx = 0;
  double R = 0;

  ros::readRosParam("/static_params/mpc/Q_q", Q_q);
  ros::readRosParam("/static_params/mpc/Q_dq", Q_dq);
  ros::readRosParam("/static_params/mpc/Q_x", Q_x);
  ros::readRosParam("/static_params/mpc/Q_dx", Q_dx);
  ros::readRosParam("/static_params/mpc/R", R);

  L(0) = Q_q;  // q
  L(1) = Q_dq; // dq
  L(2) = Q_x;  // x
  L(3) = Q_dx; // dx
  K(0) = R;

  L_qp.diagonal() = L.replicate(horizon, 1);
  K_qp.diagonal() = K.replicate(horizon, 1);
  // cout << "L_qp: " << L_qp << endl;
  // cout << "K_qp: " << K_qp << endl;
  ROS_INFO_STREAM_ONCE("K_QP: " << K_qp);

  A << 0.98495, 0.0099498, 0, 0, -3.0033, 0.98495, 0, 0, 0, 0, 1, 0.01, 0, 0, 0, 1;
  B << 9.9749e-05, 0.0199, 0.0070566, 1.4113;

  // Ad = A * _controller_dt + Eigen::Matrix<double, num_vars, num_vars>::Identity();
  // Bd = B * _controller_dt;
  Ad = A;
  Bd = B;

  // Ad_qp = Ad;
  // Bd_qp = Bd;

  Eigen::Matrix<double, num_vars, num_vars> powerMats[horizon];
  powerMats[0].setIdentity();

  // I, A, A^2...
  for (uint16_t i = 1; i < horizon; i++)
  {
    // cout << "power mats i: " << i << endl;

    powerMats[i] = Ad * powerMats[i - 1];
  }

  for (uint16_t i = 0; i < horizon; i++)
  {
    Ad_qp.block<4, 4>(i * 4, 0) = Ad * powerMats[i];

    for (uint16_t k = 0; k < horizon; k++)
    {
      if (i >= k)
      {
        uint16_t a_num = i - k;
        Bd_qp.block<4, 1>(4 * i, 1 * k) = powerMats[a_num] * Bd;
      }

      // Bd_qp.block<4, 1>(i, k) = powerMats[el] * Bd;
    }
  }

  // cout << "Ad_qp: " << Ad_qp << endl;
  // cout << "Bd_qp: " << Bd_qp << endl;

  qpOASES::real_t H_qpoases[num_u * horizon * num_u * horizon]; // nV x nV
  qpOASES::real_t g_qpoases[num_u * horizon];                   // nV
  qpOASES::real_t A_qpoases[num_cons * num_vars];               // nC x nV
  qpOASES::real_t ub_qpoases[num_cons * horizon];               // nV
  qpOASES::real_t lb_qpoases[num_cons * horizon];               // nV
  qpOASES::real_t ubA_qpoases[num_cons * horizon];              // nV
  qpOASES::real_t lbA_qpoases[num_cons * horizon];              // nV

  lb_qpoases[0] = -0.2;
  ub_qpoases[0] = 0.2;
  lbA_qpoases[0] = -0.2;
  ubA_qpoases[0] = 0.2;
  A_qpoases[0] = 1;

  x(0) = q;
  x(1) = dq;
  // x(2) = 0.0;
  x(2) = _data.p_act(0);
  x(3) = _data.vb_act(0);

  x_d(0) = 0.0;
  x_d(1) = 0.0;
  // x_d(2) = 0.0;
  x_d(2) = _cmd.p_des(0);
  x_d(3) = _cmd.v_des(0);

  for (uint16_t i = 1; i < horizon; i++)
  {
    // Eigen::Matrix<double, 4, 1> dx;
    // dx.setZero();
    // dx(2) = x_d.block<4, 1>((i - 1) * 4, 1)(2) + x_d.block<4, 1>((i - 1) * 4, 1)(3) * _controller_dt;
    x_d.block<4, 1>(i * 4, 0) = x_d.block<4, 1>((i - 1) * 4, 0);
    x_d.block<4, 1>(i * 4, 0)(2) = x_d.block<4, 1>((i - 1) * 4, 0)(2) + x_d.block<4, 1>((i - 1) * 4, 0)(3) * _controller_dt;
  }

  // x_d = x_d.block<4, 1>(0, 0).replicate(horizon, 1);

  H_qp = 2 * (Bd_qp.transpose() * L_qp * Bd_qp + K_qp);
  g_qp = 2 * Bd_qp.transpose() * L_qp * (Ad_qp * x - x_d);

  // cout << Ad * x - x_d << endl;
  // cout << "pitch des: " << pitch_des << endl;

  _matrixToReal(H_qpoases, H_qp, num_u * horizon, num_u * horizon);
  _matrixToReal(g_qpoases, g_qp, num_u * horizon, 1);

  // qpOASES::QProblemB qproblem(1);
  qpOASES::QProblem qproblem(num_u, num_cons);
  qpOASES::Options qoptions;
  // qoptions.setToMPC();
  qoptions.setToReliable();
  qoptions.printLevel = qpOASES::PL_NONE;
  qproblem.setOptions(qoptions);

  qpOASES::int_t nWSR = 1e3;

  // int rval = qproblem.init(H_qpoases, g_qpoases, lb_qpoases, ub_qpoases, nWSR);
  int rval = qproblem.init(H_qpoases, g_qpoases, A_qpoases, lb_qpoases, ub_qpoases, lbA_qpoases, ubA_qpoases, nWSR);

  // if (rval == qpOASES::SUCCESSFUL_RETURN)
  // {
  //   cout << "MPC init succsess!" << endl;
  // }
  // else if (rval == qpOASES::RET_MAX_NWSR_REACHED)
  // {
  //   cout << "MPC max NWSR reached!" << endl;
  // }
  // else if (rval == qpOASES::RET_INIT_FAILED)
  // {
  //   cout << "MPC init failed!" << endl;
  // }
  // else
  // {
  //   cout << "MPC init failed idk error!" << endl;
  // }

  qpOASES::real_t solution[num_vars * horizon];
  rval = qproblem.getPrimalSolution(solution);

  solution[0] *= -1.0;

  if (rval != qpOASES::SUCCESSFUL_RETURN)
  {
    cout << "MPC failed to solve!" << endl;
  }
  else
  {
    // cout << "mpc solution: " << solution[0] << endl;
  }

  tau = (float)solution[0];
  delta_tau = 1e-1 * (_cmd.rpy_des(2) - _data.rpy(2)) + 1e-1 * (_cmd.omega_des(2) - _data.omega_body(2));

  _writeCmd(tau, delta_tau);

  Eigen::Matrix4f Q;
  Q.setZero();
  Q(0, 0) = L(0);
  Q(1, 1) = L(1);
  Q(2, 2) = L(2);
  Q(3, 3) = L(3);

  _evalCostFunction(Q, R, x.cast<float>(), tau);
}

void Robot_Controller::_evalCostFunction(Eigen::Matrix4f Q, float R, Eigen::Vector4f x, float u)
{
  _data.cost.J += x.transpose() * Q * x + u * R * u;
}
