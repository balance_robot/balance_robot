#pragma once

#include "data_types.hpp"
#include "raisim/RaisimServer.hpp"
#include "raisim/object/singleBodies/SingleBodyObject.hpp"
#include "ros/ros.h"
#include <iostream>
#include <tf/transform_broadcaster.h>

#define LOOP_RATE 100 // Hz

class Raisim_Driver
{
public:
  Raisim_Driver(float dt);
  ~Raisim_Driver();

  void init();
  void loop();

  Data readData();
  void writeCmd(Command cmd);

private:
  float _sim_dt = 0.01; // 100 Hz default

  Eigen::VectorXd _jointTargetPosition;
  Eigen::VectorXd _jointTargetVelocity;
  Eigen::VectorXd _jointTargetEffort;

  Eigen::VectorXd _actPos;
  Eigen::VectorXd _actVel;
  Eigen::VectorXd _prevVel;
  Eigen::VectorXd _actAcc;

  raisim::ArticulatedSystem* _robot;
  raisim::World* _world;
  raisim::RaisimServer* _server;

  Data _data;
  Command _cmd;

  void _zero();
  void _spawnRobot();
  void _safetyCheck();
  void _computeImu();

  float _rand();
  float _makeNoize(float avr = 0.0, uint8_t count = 5);
};