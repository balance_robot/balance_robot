#include "raisim_driver/raisim_driver.hpp"
#include "robot_controller/robot_controller.hpp"
#include "ros/ros.h"
#include <iostream>

using std::cout;
using std::endl;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "balance_robot_node");
  ros::NodeHandle n;
  ros::Rate rate(LOOP_RATE);

  ROS_INFO("Initialization...");

  raisim::World::setActivationKey("~/.raisim/activation.raisim");

  float dt = 1.0 / (float)LOOP_RATE;

  Raisim_Driver raisim_driver(dt);
  raisim_driver.init();

  Robot_Controller robot_controller(1.0 / (float)LOOP_RATE);
  Data data;
  Command cmd;

  static unsigned long iter = 0;

  ROS_INFO("Enter loop");

  raisim_driver.loop();

  while (ros::ok())
  {
    if (iter > LOOP_RATE * 2)
    {
      data = raisim_driver.readData();
      robot_controller.setData(data);
      robot_controller.run();
      cmd = robot_controller.getControl();
      raisim_driver.writeCmd(cmd);
      raisim_driver.loop();
    }

    iter++;

    ros::spinOnce();
    rate.sleep();
  }

  return 0;
}