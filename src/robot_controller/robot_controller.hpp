#pragma once

#include "balance_robot_msgs/cmd.h"
#include "balance_robot_msgs/data.h"
#include "data_types.hpp"
#include <Eigen/Core>
#include <Eigen/Dense>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/TransformStamped.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <random>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>
#include <third_party/qpOASES/include/qpOASES.hpp>

#define PI 3.14159265359

class Robot_Controller
{
public:
  Robot_Controller(float dt);
  ~Robot_Controller();

  void run();
  void setData(Data data);
  Command getControl();
  Eigen::Matrix<float, 6, 1> getOdom();

private:
  void _initPublishers();
  void _initSubscribers();
  void _cmdVelCallback(geometry_msgs::Twist msg);
  double _getTime();
  void _matrixToReal(qpOASES::real_t* dst, Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> src, uint16_t rows, uint16_t cols);
  void _updateVisualization();
  void _trajectoryPredicton();
  void _updatePlot();
  void _evalOdom();
  void _writeCmd(float tau, float delta_tau);
  void _orientationEstimator();
  void _complimentaryFilter();
  void _extendedKalmanFilter();
  void _control1();
  void _lqr1();
  void _lqrV2C(); // model version 2 continous
  void _lqrV2H(); // model version 2 with horizon like mpc
  void _lqr_matlab(); //get A, B, K matrix from matlab
  void _mpc();
  void _evalCostFunction(Eigen::Matrix4f Q, float R, Eigen::Vector4f x, float u);
  void _filterInput();

  Data _data = { 0 };
  Command _cmd = { 0 };
  float _controller_dt = 0.01; // 100 Hz default
  nav_msgs::Path _referenceTrajectory;
  geometry_msgs::Point _p_start;
  Eigen::Vector3f _rpy_start;
  Eigen::Vector3f _p_odom;
  bool _is_frist_run = true;
  ros::Time t_start;
  Params _params;

  ros::NodeHandle _nh;

  ros::Subscriber _sub_cmd_vel;
  ros::Publisher _pub_joint_states;
  ros::Publisher _pub_cmd;
  ros::Publisher _pub_data;
  ros::Publisher _pub_odom;
  ros::Publisher _pub_imu;
  ros::Publisher _pub_ground_truth_odom;
  ros::Publisher _pub_ref_traj;
  tf::TransformBroadcaster _odom_broadcaster;
  tf::TransformBroadcaster _tf_world_odom_broadcaster;
};

namespace ros
{

inline Eigen::Vector3f pointToVector3f(geometry_msgs::Point point)
{
  Eigen::Vector3f vec;

  vec(0) = point.x;
  vec(1) = point.y;
  vec(2) = point.z;

  return vec;
}

template<typename T>
bool readRosParam(std::string param_name, T& param_var)
{
  if (!ros::param::get(param_name, param_var))
  {
    ROS_WARN_STREAM("Can't read param " << param_name);
    return false;
  }

  // std::cout << "[ROS PARAM] " << param_name << ": " << param_var << std::endl;

  return true;
}

}

namespace Eigen
{

inline geometry_msgs::Point vector3fToPoint(Eigen::Vector3f vec)
{
  geometry_msgs::Point point;

  point.x = vec(0);
  point.y = vec(1);
  point.z = vec(2);

  return point;
}

}
