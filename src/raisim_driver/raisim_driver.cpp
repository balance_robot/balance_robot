#include "raisim_driver/raisim_driver.hpp"

using std::cout;
using std::endl;

Raisim_Driver::Raisim_Driver(float dt)
  : _jointTargetPosition(9),
    _jointTargetVelocity(8),
    _jointTargetEffort(8),
    _actPos(9),
    _actVel(8),
    _prevVel(8),
    _actAcc(8)
{
  _sim_dt = dt;
}

Raisim_Driver::~Raisim_Driver()
{
  _server->killServer();
}

void Raisim_Driver::init()
{
  _world = new raisim::World;
  _server = new raisim::RaisimServer(_world);

  _zero();

  _spawnRobot();
}

void Raisim_Driver::_spawnRobot()
{
  _world->setTimeStep(_sim_dt);
  _world->addGround();

  tf::Quaternion start_quat(tf::Vector3(0, 1, 0), 5.0 * 3.1415 / 180.0);
  // tf::Quaternion start_quat(tf::Vector3(0, 0, 1), 90.0 * 3.1415 / 180.0);

  geometry_msgs::Point p_start;
  p_start.x = 0.05;
  p_start.y = 0.0;
  p_start.z = 0.158;

  // body spawn coordinates
  _jointTargetPosition[0] = p_start.x; // root x
  _jointTargetPosition[1] = p_start.y; // root y
  _jointTargetPosition[2] = p_start.z; // root z

  // jointTargetPosition[3] = 1.0;   // w of quaternion
  // jointTargetPosition[4] = 0.0;   // x of quaternion
  // jointTargetPosition[5] = 0.0;   // y of quaternion
  // jointTargetPosition[6] = 0.0;   // z of quaternion

  _jointTargetPosition[3] = start_quat.getW(); // w of quaternion
  _jointTargetPosition[4] = start_quat.getX(); // x of quaternion
  _jointTargetPosition[5] = start_quat.getY(); // y of quaternion
  _jointTargetPosition[6] = start_quat.getZ(); // z of quaternion

  _robot = _world->addArticulatedSystem("/home/splitmind/balance_robot_ws/src/balance_robot_description/urdf/balance_robot.urdf");
  _robot->setName("robot");

  cout << "Robot DOF: " << _robot->getDOF() << endl;

  _robot->setGeneralizedCoordinate(_jointTargetPosition);
  _robot->setGeneralizedVelocity(_jointTargetVelocity);
  _robot->setGeneralizedVelocity(_jointTargetVelocity);
  _robot->setGeneralizedForce(Eigen::VectorXd::Zero(_robot->getDOF()));
  _robot->setControlMode(raisim::ControlMode::PD_PLUS_FEEDFORWARD_TORQUE);

  Eigen::Vector3f box_size(1, 2, 0.07);
  // Eigen::Vector3f box_pos(1, 0, box_size(2) / 2);
  // Eigen::Vector3f box_pos(1, 0, 0);
  tf::Quaternion box_quat(tf::Vector3(1, 0, 0), -10.0 * 3.1415 / 180.0);

  auto block_2 = _world->addBox(box_size(0), box_size(1), box_size(2), 0);
  block_2->setName("block_2");
  block_2->setBodyType(raisim::BodyType::STATIC);
  block_2->setPosition(0, -2, 0);
  block_2->setOrientation(box_quat.getW(), box_quat.getX(), box_quat.getY(), box_quat.getZ());
  block_2->setAppearance("1.0, 1.0, 1.0, 1.0");

  _server->launchServer();
  _server->focusOn(_robot);
}

void Raisim_Driver::loop()
{
  _server->integrateWorldThreadSafe();
  _robot->getState(_actPos, _actVel);

  _data.r_q = _actPos.tail(2)[0];
  _data.l_q = _actPos.tail(2)[1];
  _data.r_dq = _actVel.tail(2)[0];
  _data.l_dq = _actVel.tail(2)[1];

  _data.p_act(0) = _actPos(0);
  _data.p_act(1) = _actPos(1);
  _data.p_act(2) = _actPos(2);

  _data.vw_act(0) = _actVel(0);
  _data.vw_act(1) = _actVel(1);
  _data.vw_act(2) = _actVel(2);

  _data.quat(0) = _actPos(3); // w
  _data.quat(1) = _actPos(4); // x
  _data.quat(2) = _actPos(5); // y
  _data.quat(3) = _actPos(6); // z

  tf::Quaternion quat;
  quat.setX(_data.quat(1));
  quat.setY(_data.quat(2));
  quat.setZ(_data.quat(3));
  quat.setW(_data.quat(0));

  double roll = 0;
  double pitch = 0;
  double yaw = 0;

  tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);
  tf::Matrix3x3 Rot(quat);
  // YXZ notation
  // Rot = Rz * Rx * Ry

  _data.rpy(0) = (float)roll;
  _data.rpy(1) = (float)pitch;
  _data.rpy(2) = (float)yaw;

  for (uint8_t i = 0; i < 3; i++)
  {
    for (uint8_t j = 0; j < 3; j++)
    {
      _data.R(i, j) = Rot[i][j];
    }
  }

  _data.omega_world(0) = _actVel(3);
  _data.omega_world(1) = _actVel(4);
  _data.omega_world(2) = _actVel(5);

  _data.omega_body = _data.R.transpose() * _data.omega_world;
  _data.vb_act = _data.R.transpose() * _data.vw_act;

  _computeImu();

  _safetyCheck();
}

Data Raisim_Driver::readData()
{
  return _data;
}

void Raisim_Driver::writeCmd(Command cmd)
{
  _cmd = cmd;

  _jointTargetEffort.tail(2)(0) = _cmd.r_tau;
  _jointTargetEffort.tail(2)(1) = _cmd.l_tau;
  _robot->setGeneralizedForce(_jointTargetEffort);
}

void Raisim_Driver::_zero()
{
  _jointTargetPosition.setZero();
  _jointTargetVelocity.setZero();
  _jointTargetEffort.setZero();

  _actPos.setZero();
  _actVel.setZero();
  _prevVel.setZero();
  _actAcc.setZero();
}

void Raisim_Driver::_safetyCheck()
{
  float max_angle = 80.0 * 3.1415 / 180.0;

  if (abs(_data.rpy(0)) > max_angle || abs(_data.rpy(1)) > max_angle)
  {
    _jointTargetEffort.tail(2)(0) = 0.0;
    _jointTargetEffort.tail(2)(1) = 0.0;

    _robot->setGeneralizedVelocity(_jointTargetVelocity);
    _robot->setGeneralizedForce(_jointTargetEffort);

    // ROS_ERROR("FALL");
  }
}

void Raisim_Driver::_computeImu()
{
  for (size_t i = 0; i < 3; i++)
  {
    _actAcc(i) = (_actVel(i) - _prevVel(i)) / _sim_dt;
    _prevVel(i) = _actVel(i);
  }

  _data.accw_act(0) = _actAcc(0);
  _data.accw_act(1) = _actAcc(1);
  _data.accw_act(2) = _actAcc(2) - 9.81;

  _data.accb_act = _data.R.transpose() * _data.accw_act;

  // noise
  // _data.acceleration = _data.accb_act + 0.5 * Eigen::Vector3f(_makeNoize(), _makeNoize(), _makeNoize());
  // _data.gyroscope = _data.omega_body + 0.15 * Eigen::Vector3f(_makeNoize(), _makeNoize(), _makeNoize());

  // no noise
  _data.acceleration = _data.accb_act;
  _data.gyroscope = _data.omega_body;
}

float Raisim_Driver::_rand()
{
  std::random_device rand_dev;
  std::mt19937 generator(rand_dev());
  std::uniform_real_distribution<float> dist(0.0, 1.0);

  // std::cout << dist(generator) << endl;
  return dist(generator);
}

float Raisim_Driver::_makeNoize(float avr, uint8_t count)
{
  float noize = 0.0;

  for (uint8_t i = 0; i < count; i++)
  {
    noize += (_rand() - 0.5) * 2.0;
  }

  return noize / (float)count;
}
