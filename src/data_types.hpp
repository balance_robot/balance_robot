#pragma once
#include <Eigen/Core>

struct CostFunction
{
  float J;
  Eigen::Matrix4f Q;
  float R;
};

struct Params
{
  float m;
  float r;
  float Jw;
  float M;
  float h;
};

struct Data
{
  float r_q;
  float r_dq;
  float l_q;
  float l_dq;

  Eigen::Vector3f p_act;
  Eigen::Vector3f vb_act;
  Eigen::Vector3f vw_act;
  Eigen::Vector3f accb_act;
  Eigen::Vector3f accw_act;
  Eigen::Vector4f quat;
  Eigen::Vector3f rpy;
  Eigen::Vector3f omega_world;
  Eigen::Vector3f omega_body;
  Eigen::Matrix3f R;
  Eigen::Matrix3f R_odom;

  Eigen::Vector3f p_odom;
  Eigen::Vector3f v_odom;
  Eigen::Vector3f rpy_odom;
  Eigen::Vector3f omega_odom;
  Eigen::Vector3f acceleration;
  Eigen::Vector3f gyroscope;

  CostFunction cost;
};

struct Command
{
  float r_tau;
  float l_tau;

  Eigen::Vector3f p_des;
  Eigen::Vector3f v_des;
  Eigen::Vector3f v_cmd;
  Eigen::Vector3f rpy_des;
  Eigen::Vector3f omega_des;
};